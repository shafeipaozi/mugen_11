#!/usr/bin/bash

# Copyright (c) 2023 Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   zhaozhenyang
# @Contact   :   zhaozhenyang@uniontech.com
# @Date      :   2023.2.17
# @License   :   Mulan PSL v2
# @Desc      :   使用popt库输出命令的帮助信息
# ############################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh

function pre_test() {
    LOG_INFO "Start environment preparation."
    DNF_INSTALL "popt popt-devel gcc"
    LOG_INFO "End of environmental preparation!"
}

function run_test() {
    LOG_INFO "Start testing..."
    cat >parse.c <<EOF
#include <popt.h>
#include <stdio.h>
#include <stdlib.h>

void usage (poptContext optCon , int exitcode , char *error , char *addl ) {
poptPrintUsage (optCon , stderr , 0 ) ;
if (error ) fprintf (stderr , "%s: %s\n" , error , addl ) ;
exit (exitcode ) ;
}

int main ( int argc , char *argv [ ] ) {
int c ; /* used for argument parsing */
int i = 0 ; /* used for tracking options */
int speed = 0 ; /* used in argument parsing to set speed */
int raw = 0 ; /* raw mode? */
int j ;
char buf [BUFSIZ + 1 ] ;
const char *portname ;
poptContext optCon ; /* context for parsing command-line options */

struct poptOption optionsTable [ ] = {

{ "bps" , 'b' , POPT_ARG_INT , &speed , 0 ,
"signaling rate in bits-per-second" , "BPS" } ,
{ "crnl" , 'c' , 0 , 0 , 'c' ,
"expand cr characters to cr/lf sequences" , NULL } ,
{ "hwflow" , 'h' , 0 , 0 , 'h' ,
"use hardware (RTS/CTS) flow control" , NULL } ,
{ "noflow" , 'n' , 0 , 0 , 'n' ,
"use no flow control" , NULL } ,
{ "raw" , 'r' , 0 , &raw , 0 ,
"don't perform any character conversions" , NULL } ,
{ "swflow" , 's' , 0 , 0 , 's' ,
"use software (XON/XOF) flow control" , NULL } ,
POPT_AUTOHELP
{ NULL , 0 , 0 , NULL , 0 }
} ;

optCon = poptGetContext (NULL , argc , ( const char ** )argv , optionsTable , 0 ) ;
poptSetOtherOptionHelp (optCon , "[OPTIONS]* <port>" ) ;

if (argc < 2 ) {
poptPrintUsage (optCon , stderr , 0 ) ;
exit ( 1 ) ;
}

/* Now do options processing, get portname */
while ( (c = poptGetNextOpt (optCon ) ) >= 0 ) {
switch (c ) {

case 'c' :
buf [i ++ ] = 'c' ;
break ;
case 'h' :
buf [i ++ ] = 'h' ;
break ;
case 's' :
buf [i ++ ] = 's' ;
break ;
case 'n' :
buf [i ++ ] = 'n' ;
break ;
}
}
portname = poptGetArg (optCon ) ;
if ( (portname == NULL ) || ! (poptPeekArg (optCon ) == NULL ) )
usage (optCon , 1 , "Specify a single port" , ".e.g., /dev/cua0" ) ;

if (c < - 1 ) {
/* an error occurred during option processing */
fprintf (stderr , "%s: %s\n" ,
poptBadOption (optCon , POPT_BADOPTION_NOALIAS ) ,
poptStrerror (c ) ) ;
return 1 ;
}

/* Print out options, portname chosen */

printf ( "Options chosen: " ) ;
for (j = 0 ; j < i ; j ++ )
printf ( "-%c " , buf [j ] ) ;
if (raw ) printf ( "-r " ) ;
if (speed ) printf ( "-b %d " , speed ) ;
printf ( "\nPortname chosen: %s\n" , portname ) ;

poptFreeContext (optCon ) ;
exit ( 0 ) ;
}
EOF
    gcc -Wall -o parse parse.c -lpopt
    CHECK_RESULT $? 0 0 "c file failed to compile"
    ./parse >test.log 2>&1
    grep Usage test.log
    CHECK_RESULT $? 0 0 "Unable to view parse usage"
    ./parse -?
    CHECK_RESULT $? 0 0 "The parse usage fails to be applied"
    LOG_INFO "Finish test!"
}

function post_test() {
    LOG_INFO "start environment cleanup."
    rm -rf parse* test.log
    DNF_REMOVE "$@"
    LOG_INFO "Finish environment cleanup!"
}

main "$@"
